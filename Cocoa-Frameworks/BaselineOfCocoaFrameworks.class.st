Class {
	#name : #BaselineOfCocoaFrameworks,
	#superclass : #BaselineOf,
	#category : #'Cocoa-Frameworks'
}

{ #category : #baselines }
BaselineOfCocoaFrameworks >> baseline: spec [
	<baseline>
	spec for: #common do: [ 
		spec package: 'Cocoa-Frameworks' ]
]
