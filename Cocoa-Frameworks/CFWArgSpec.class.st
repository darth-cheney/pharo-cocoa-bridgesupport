"
I represent the specification of a Cocoa function or method argument, as written in an Apple '.bridgesupport' XML file.
"
Class {
	#name : #CFWArgSpec,
	#superclass : #CFWSpec,
	#instVars : [
		'c_array_length_in_arg',
		'index',
		'type_modifier',
		'printf_format',
		'function_pointer',
		'sel_of_type64',
		'c_array_of_fixed_length',
		'type',
		'type64',
		'sel_of_type',
		'args',
		'retvals'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #'as yet unclassified' }
CFWArgSpec >> args [

^ args
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> args: anOrderedCollection [
	args := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> cArrayLengthInArg [
	^ c_array_length_in_arg
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> cArrayLengthInArg: aCArrayLengthInArg [
	c_array_length_in_arg := aCArrayLengthInArg
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> cArrayOfFixedLength [
	^ c_array_of_fixed_length
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> cArrayOfFixedLength: aCArrayOfFixedLength [
	c_array_of_fixed_length := aCArrayOfFixedLength
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> functionPointer [
	^ function_pointer
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> functionPointer: aFunctionPointer [
	function_pointer := aFunctionPointer
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> index [
	^ index
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> index: aIndex [
	index := aIndex
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> initialize [
	super initialize.
	args := OrderedCollection new.
	retvals := OrderedCollection new.
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> printfFormat [
	^ printf_format
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> printfFormat: aPrintfFormat [
	printf_format := aPrintfFormat
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> retvals [

^ retvals
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> retvals: anOrderedCollection [
	retvals := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> selOfType [
	^ sel_of_type
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> selOfType64 [
	^ sel_of_type64
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> selOfType64: aSelOfType64 [
	sel_of_type64 := aSelOfType64
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> selOfType: aSelOfType [
	sel_of_type := aSelOfType
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> type [
	^ type
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> type64 [
	^ type64
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> type64: aType64 [
	type64 := aType64
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> type: aType [
	type := aType
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> typeModifier [
	^ type_modifier
]

{ #category : #'as yet unclassified' }
CFWArgSpec >> typeModifier: aTypeModifier [
	type_modifier := aTypeModifier
]
