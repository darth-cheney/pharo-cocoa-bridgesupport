"
I am an object that can parse Apple '.bridgesupport' XML files and create the appropriate CFWSpec
instances for the elements enumerated within.

CFWSpec classes are designed to hold relevant Bridge Support information and we hope can eventually be used to create Cocoa proxy classes into Pharo.
"
Class {
	#name : #CFWParser,
	#superclass : #Object,
	#instVars : [
		'cocoaClasses',
		'xmlDoc',
		'supportPath',
		'libPath'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #'instance creation' }
CFWParser class >> fromSupportFilePath: aString [
	"Respond with a new instance of self with the
	bridgesupport file path set"
	^ self new supportPath: aString
]

{ #category : #parsing }
CFWParser class >> parseSupportFilePath: aString [
	| inst |
	inst := self fromSupportFilePath: aString.
	inst parse.
	^ inst
]

{ #category : #'as yet unclassified' }
CFWParser >> classSpecs [
	^ cocoaClasses
]

{ #category : #initialization }
CFWParser >> initialize [
	super initialize.
	cocoaClasses := OrderedCollection new.
]

{ #category : #accessing }
CFWParser >> libPath: aString [
	"Absolute path to the dylib file for the framework"
	aString asFileReference exists
		ifTrue: [ libPath := aString ].
	^ self
]

{ #category : #parsing }
CFWParser >> parse [
	| doc classElements |
	supportPath ifNil: [ ^ nil ].
	doc := XMLDOMParser parse: (supportPath asFileReference).
	classElements := doc root allElementsSelect: [ :el |
		el name = 'class' ].
	cocoaClasses := classElements collect: [ :classElement |
		| newSpec |
		newSpec := CFWClassSpec new.
		newSpec sourceElement: classElement.
		self
			setAttributesOf: newSpec
			fromElement: classElement;
			parseMethodsFor: newSpec 
			fromElement: classElement.
		newSpec ]
	
]

{ #category : #parsing }
CFWParser >> parseArgsFor: aCFWSpec fromElement: anXMLElement [
	"For a given CFWSpec object, create and parse individual
	args elements"
	| argElements |
	argElements := anXMLElement allElementsSelect: [ :el | 
		el name = 'arg' and: [ el parent = anXMLElement ] ].
	aCFWSpec args: (argElements collect: [ :argElement |
		| newSpec |
		newSpec := CFWArgSpec new.
		newSpec sourceElement: argElement.
		self
			setAttributesOf: newSpec
			fromElement: argElement.
		argElement hasChildren
			ifTrue: [ 
				self
					parseArgsFor: newSpec
					fromElement: argElement;
					parseRetvalsFor: newSpec
					fromElement: argElement ].
		newSpec ]).
]

{ #category : #parsing }
CFWParser >> parseMethodsFor: aCFWClassSpec fromElement: anXMLElement [
	"Create an constituent CFWMethodSpec instances
	based on children methods in the XML element (which
	should be a class element)"
	| methodElements |
	methodElements := anXMLElement allElementsSelect: [ :el |
		el name = 'method' ].
	aCFWClassSpec 
		methods: (methodElements collect: [ :methodElement | 
			| newSpec |
			newSpec := CFWMethodSpec new.
			newSpec sourceElement: methodElement.
			self
				setAttributesOf: newSpec
				fromElement: methodElement.
			self
				parseArgsFor: newSpec
				fromElement: methodElement;
				parseRetvalsFor: newSpec
				fromElement: methodElement.
			newSpec ]).
]

{ #category : #parsing }
CFWParser >> parseRetvalsFor: aCFWSpec fromElement: anXMLElement [
	"For a given CFWSpec object, create and parse individual
	args elements"
	| retvalElements |
	retvalElements := anXMLElement allElementsSelect: [ :el | 
		el name = 'retval' and: [ el parent = anXMLElement ] ].
	aCFWSpec retvals: (retvalElements collect: [ :retvalElement |
		| newSpec |
		newSpec := CFWRetvalSpec new.
		newSpec sourceElement: retvalElement.
		self
			setAttributesOf: newSpec
			fromElement: retvalElement.
		retvalElement hasChildren
			ifTrue: [ 
				self
					parseArgsFor: newSpec
					fromElement: retvalElement;
					parseRetvalsFor: newSpec
					fromElement: retvalElement ].
		newSpec ]).
]

{ #category : #initialization }
CFWParser >> setAttributesOf: aCFWSpecInstance fromElement: anXMLElement [
	"Goes through the attributes of the passed in
	xml element and uses the getters of the passed
	cfw instance to set the values appropriately"
	anXMLElement attributes keysAndValuesDo: [ :key :val |
		| selectorName |
		selectorName := key copy.
		selectorName := (' ' join: (selectorName splitOn: '_')).
		selectorName := selectorName asCamelCase.
		selectorName at: 1 put: ((selectorName at: 1) lowercase).
		selectorName := selectorName,':'.
		aCFWSpecInstance perform: selectorName asSymbol with: val ].
	
]

{ #category : #accessing }
CFWParser >> supportPath: aString [
	"Absolute path to an XML bundlesupport file"
	aString asFileReference exists
		ifTrue: [ 
			supportPath := aString ].
	^ self
]
