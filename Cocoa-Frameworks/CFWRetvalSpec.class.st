"
I represent the specification of a Cocoa function or method return value, as written in an Apple '.bridgesupport' XML file.
"
Class {
	#name : #CFWRetvalSpec,
	#superclass : #CFWSpec,
	#instVars : [
		'function_pointer',
		'type64',
		'type',
		'args',
		'retvals'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> args [

^ args
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> args: anOrderedCollection [
	args := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> functionPointer [
	^ function_pointer
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> functionPointer: aFunctionPointer [
	function_pointer := aFunctionPointer
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> initialize [
	super initialize.
	args := OrderedCollection new.
	retvals := OrderedCollection new.
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> retvals [

^ retvals
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> retvals: anOrderedCollection [
	retvals := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> type [
	^ type
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> type64 [
	^ type64
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> type64: aType64 [
	type64 := aType64
]

{ #category : #'as yet unclassified' }
CFWRetvalSpec >> type: aType [
	type := aType
]
