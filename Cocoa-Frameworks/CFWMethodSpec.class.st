"
I represent the specification of a Cocoa method, as written in an Apple '.bridgesupport' XML file.
"
Class {
	#name : #CFWMethodSpec,
	#superclass : #CFWSpec,
	#instVars : [
		'type64',
		'selector',
		'type',
		'class_method',
		'variadic',
		'args',
		'retvals'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #'as yet unclassified' }
CFWMethodSpec >> args [

^ args
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> args: anOrderedCollection [
	args := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> classMethod [
	^ class_method
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> classMethod: aClassMethod [
	class_method := aClassMethod
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> initialize [
	super initialize.
	args := OrderedCollection new.
	retvals := OrderedCollection new.
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> retvals [

^ retvals
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> retvals: anOrderedCollection [
	retvals := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> selector [
	^ selector
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> selector: aSelector [
	selector := aSelector
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> type [
	^ type
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> type64 [
	^ type64
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> type64: aType64 [
	type64 := aType64
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> type: aType [
	type := aType
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> variadic [
	^ variadic
]

{ #category : #'as yet unclassified' }
CFWMethodSpec >> variadic: aVariadic [
	variadic := aVariadic
]
