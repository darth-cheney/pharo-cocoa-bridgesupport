"
I am the parent class of a ""specification"" that is present in an Apple '.bridgesupport' XML file.

My descendant instances are created by using CFWParse on one of these XML files.

The hope is that I can be used to eventually create Cocoa proxy objects in Pharo.
"
Class {
	#name : #CFWSpec,
	#superclass : #Object,
	#instVars : [
		'sourceElement'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #initialization }
CFWSpec >> initialize [
	super initialize
]

{ #category : #accessing }
CFWSpec >> sourceElement [
	^ sourceElement
]

{ #category : #accessing }
CFWSpec >> sourceElement: anXMLElement [
	sourceElement := anXMLElement
]
