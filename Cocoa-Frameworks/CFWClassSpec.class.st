"
I represent the specification of a Cocoa class, as written in an Apple '.bridgesupport' XML file.
"
Class {
	#name : #CFWClassSpec,
	#superclass : #CFWSpec,
	#instVars : [
		'name',
		'methods'
	],
	#category : #'Cocoa-Frameworks-Parsing'
}

{ #category : #'as yet unclassified' }
CFWClassSpec >> initialize [
	super initialize.
	methods := OrderedCollection new.
]

{ #category : #'as yet unclassified' }
CFWClassSpec >> methods [

^ methods
]

{ #category : #'as yet unclassified' }
CFWClassSpec >> methods: anOrderedCollection [
	methods := anOrderedCollection.
]

{ #category : #'as yet unclassified' }
CFWClassSpec >> name [
	^ name
]

{ #category : #'as yet unclassified' }
CFWClassSpec >> name: aName [
	name := aName
]
